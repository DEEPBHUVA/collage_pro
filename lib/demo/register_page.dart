import 'package:collage_poj/demo/login_page.dart';
import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {

  TextEditingController nameController = TextEditingController();
  TextEditingController passController = TextEditingController();
  TextEditingController mobielController = TextEditingController();
  TextEditingController countryController = TextEditingController();
  final fomkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                Opacity(
                  opacity: 1.0,
                  child: ClipPath(
                    clipper: GetWaveClipper(),
                    child: Container(
                      width: double.infinity,
                      child: Image.asset("assets/images/register_image.jpg",
                          fit: BoxFit.cover),
                      color: Colors.deepOrange,
                      height: 350,
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    color: Color.fromARGB(255, 255, 255, 255),
                  ),
                  margin: EdgeInsets.only(left: 15, top: 15),
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Icon(Icons.chevron_left, color: Colors.black),
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(left: 20, top: 20),
              child: Row(
                children: [
                  Text(
                    "Register",
                    style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                        fontFamily: "Rubik"),
                  ),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(right: 20),
                      alignment: Alignment(1.0, 0.0),
                      child: InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          "Go To Login",
                          style: TextStyle(
                              color: Colors.black,
                              fontFamily: "Rubik",
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: EdgeInsets.all(20),
              child: Form(
                key: fomkey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    getTextFormfilled("Enter User Name", "UserName",nameController,"Please enter name"),
                    SizedBox(
                      height: 10,
                    ),
                    getTextFormfilled("Enter Password Name", "Password",passController,"Please Enter Password"),
                    SizedBox(
                      height: 10,
                    ),
                    getTextFormfilled("Enter Phone Number", "Mobile",mobielController,"Please Enter Mobile number"),
                    SizedBox(
                      height: 10,
                    ),
                    getTextFormfilled("Enter Country", "Country",countryController,"Enter Country Name"),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          child: TextButton(
                            onPressed: () {},
                            child: Container(
                              width: 200,
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: Colors.black12),
                              child: Center(
                                child: Row(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(50),
                                        color: Color.fromARGB(255, 255, 255, 255),
                                      ),
                                      child: Icon(Icons.male)
                                    ),
                                    SizedBox(
                                      width: 8,
                                    ),
                                    Text(
                                      "Male",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 15,
                                        fontFamily: "Rubik",
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: TextButton(
                            onPressed: () {},
                            child: Container(
                              width: 200,
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: Colors.black12),
                              child: Center(
                                child: Row(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(50),
                                        color: Color.fromARGB(255, 255, 255, 255),
                                      ),
                                        child: Icon(Icons.female),
                                    ),
                                    SizedBox(
                                      width: 8,
                                    ),
                                    Text(
                                      "Female",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 15,
                                        fontFamily: "Rubik",
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextButton(
                      onPressed: () {
                        if (fomkey.currentState!.validate()) {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) {
                                return LoginPage();
                              },
                            ),
                          );
                        }
                      },
                      child: Container(
                        width: 300,
                        padding: EdgeInsets.only(top: 15, bottom: 15),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.pink),
                        child: Center(
                          child: Text(
                            "Register",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                              fontFamily: "Rubik",
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class GetWaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    debugPrint(size.width.toString());
    var path = new Path();
    path.lineTo(0, size.height);
    var fstart = Offset(size.width - (size.width / 1.2), size.height - 150);
    var fend = Offset(size.width / 2, size.height - 40);
    path.quadraticBezierTo(fstart.dx, fstart.dy, fend.dx, fend.dy);

    var sstart = Offset(size.width - (size.width / 4.5), size.height - 200);
    var send = Offset(size.width + 25, (size.height));
    path.quadraticBezierTo(sstart.dx, sstart.dy, send.dx, send.dy);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper oldClipper) {
    return true;
  }
}

Widget getTextFormfilled(hinttext, labletext,control,text) {
  return TextFormField(
    controller: control,
    validator: (value) {
      if(value!=null||value!.isEmpty){
        return text;
      }
    },
    decoration: InputDecoration(
      hintText: hinttext,
      labelText: labletext,
      floatingLabelStyle: TextStyle(color: Colors.pink),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.pink),
        borderRadius: BorderRadius.circular(20),
      ),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
      ),
    ),
  );
}
