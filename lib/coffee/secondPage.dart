import 'package:flutter/material.dart';

class CoffeeSecondPage extends StatefulWidget {
  const CoffeeSecondPage({Key? key}) : super(key: key);

  @override
  State<CoffeeSecondPage> createState() => _CoffeeSecondPageState();
}

class _CoffeeSecondPageState extends State<CoffeeSecondPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey[900],
        body: Column(
          children: [
            Expanded(
              child: Stack(
                fit: StackFit.expand,
                clipBehavior: Clip.none,
                children: [
                  Image.asset(
                    'assets/coffeeImage/coffee8.jpg',
                    fit: BoxFit.fitWidth,
                  ),
                  Align(
                    alignment: AlignmentDirectional.topCenter,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 20, left: 20),
                          padding: EdgeInsets.all(10.0),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.black),
                          child: InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.chevron_left,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 20,
                            right: 20,
                          ),
                          padding: EdgeInsets.all(10.0),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.black),
                          child: Icon(
                            Icons.favorite,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: 275,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20.0),
                      child: Container(
                        height: 150,
                        width: 355,
                        decoration: BoxDecoration(
                          color: Color.fromARGB(200, 100, 100, 100),
                        ),
                        child: Container(
                          padding: EdgeInsets.all(20.0),
                          child: Column(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Cappuccion",
                                          style: TextStyle(
                                              fontFamily: "Rubik",
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20.0,
                                              color: Colors.white),
                                        ),
                                        SizedBox(
                                          height: 5.0,
                                        ),
                                        Text(
                                          "With oat Milk",
                                          style: TextStyle(
                                            fontFamily: "Rubik",
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15.0,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(left: 10),
                                          padding: EdgeInsets.all(5.0),
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(5.0),
                                            color: Colors.black87,
                                          ),
                                          child: Column(
                                            children: [
                                              Icon(
                                                Icons.schedule_send_rounded,
                                                color: Colors.orange,
                                              ),
                                              Text(
                                                "Cofee",
                                                style: TextStyle(
                                                  color: Colors.grey,
                                                  fontSize: 10.0,
                                                  fontFamily: "Rubik",
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(left: 20),
                                          padding: EdgeInsets.all(5.0),
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(5.0),
                                            color: Colors.black87,
                                          ),
                                          child: Column(
                                            children: [
                                              Icon(
                                                Icons.schedule_send_rounded,
                                                color: Colors.orange,
                                              ),
                                              Text(
                                                "Cofee",
                                                style: TextStyle(
                                                  color: Colors.grey,
                                                  fontSize: 10.0,
                                                  fontFamily: "Rubik",
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 25.0,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: Row(
                                      children: [
                                        Icon(
                                          Icons.star,
                                          color: Colors.orange,
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          "4.5",
                                          style: TextStyle(
                                              fontSize: 20.0,
                                              fontFamily: "Rubik",
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        SizedBox(
                                          width: 5.0,
                                        ),
                                        Text(
                                          "(6.986)",
                                          style: TextStyle(
                                              fontSize: 15.0,
                                              fontFamily: "Rubik",
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(10.0),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5.0),
                                      color: Colors.black87,
                                    ),
                                    child: Text(
                                      "Medium Rosted",
                                      style: TextStyle(
                                        color: Colors.grey,
                                        fontFamily: "Rubik",
                                        fontSize: 15.0,
                                      ),
                                    ),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.all(20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(
                      height: 60,
                    ),
                    Text(
                      "Description",
                      style: TextStyle(
                          fontFamily: "Rubik",
                          fontSize: 20.0,
                          color: Colors.white),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text:
                                "A Cappuccion is a coffee-based drink made\npriamarily form espresso and milk...",
                            style: TextStyle(
                              fontFamily: 'Rubik',
                              fontSize: 13.0,
                              color: Colors.grey,
                            ),
                          ),
                          TextSpan(
                            text: "Read more",
                            style: TextStyle(
                              fontFamily: 'Rubik',
                              fontSize: 13.0,
                              color: Colors.orangeAccent,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              border: Border.all(
                                color: Colors.orange,
                                style: BorderStyle.solid,
                                width: 1.0,
                              ),
                            ),
                            child: Text(
                              "S",
                              style: TextStyle(
                                color: Colors.orange,
                                fontFamily: 'Rubik',
                                fontSize: 20.0,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.black26,
                            ),
                            child: Text(
                              "M",
                              style: TextStyle(
                                color: Colors.grey,
                                fontFamily: 'Rubik',
                                fontSize: 20.0,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.black26,
                            ),
                            child: Text(
                              "L",
                              style: TextStyle(
                                color: Colors.grey,
                                fontFamily: 'Rubik',
                                fontSize: 20.0,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            children: [
                              Text(
                                "Price",
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "Rubik",
                                ),
                              ),
                              SizedBox(
                                height: 8.0,
                              ),
                              Text(
                                "\$4.20",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "Rubik",
                                  fontSize: 20.0,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.fromLTRB(30, 20, 30, 20),
                            decoration: BoxDecoration(
                              color: Colors.orange,
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Text(
                              "Buy Now",
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'Rubik',
                                fontSize: 20.0,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          flex: 3,
                        )
                      ],
                    ),
                    // Container(
                    //   height: 375,
                    //   child: ListView(
                    //     scrollDirection: Axis.horizontal,
                    //     children: [
                    //       listView(
                    //         "coffee2.jpg",
                    //       ),
                    //       listView(
                    //         "coffee1.jpg",
                    //       ),
                    //       listView(
                    //         "coffee3.jpg",
                    //       ),
                    //       listView(
                    //         "coffee4.jpg",
                    //       ),
                    //       listView(
                    //         "coffee5.jpg",
                    //       ),
                    //     ],
                    //   ),
                    // ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

// Widget listView(var imagename) {
//   return Container(
//     margin: EdgeInsets.only(right: 15.0),
//     padding: EdgeInsets.all(15.0),
//     width: 200,
//     decoration: BoxDecoration(
//       color: Colors.black26,
//       borderRadius: BorderRadius.circular(20.0),
//     ),
//     child: Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         ClipRRect(
//           borderRadius: BorderRadius.circular(20.0),
//           child: Image.asset(
//             "assets/coffeeImage/" + imagename,
//             height: 250,
//           ),
//         ),
//       ],
//     ),
//   );
// }
