import 'package:collage_poj/coffee/secondPage.dart';
import 'package:collage_poj/demo/front_page.dart';
import 'package:flutter/material.dart';

class FirstPage extends StatefulWidget {
  const FirstPage({Key? key}) : super(key: key);

  @override
  State<FirstPage> createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: Container(
          margin: EdgeInsets.only(left: 20),
          child: InkWell(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return FrontPage();
              },));
            },
            child: Icon(
              Icons.menu,
              color: Colors.white,
            ),
          ),
        ),
        actions: [
          Container(
            margin: EdgeInsets.only(right: 20),
            child: Icon(
              Icons.person,
              color: Colors.white,
            ),
          )
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            backgroundColor: Colors.black,
            icon: Icon(
              Icons.home,
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.shopping_bag_rounded,
            ),
            label: 'Shopping',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.favorite,
              // color: Colors.orange,
            ),
            label: 'Favorite',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.notification_important,
            ),
            label: 'Notification',
          ),
        ],
      ),
      body: Container(
        margin: EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Find the best\ncoffee for you",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 30.0,
                  fontFamily: "Rubik",
                ),
              ),
              SizedBox(
                height: 20,
              ),
              TextField(
                decoration: InputDecoration(
                  hintText: "Find your Coffee...",
                  hintStyle: TextStyle(
                    color: Colors.grey,
                  ),
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.orangeAccent,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.grey.shade600,
                    ),
                    borderRadius: BorderRadius.circular(
                      20.0,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.grey.shade600,
                    ),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                height: 20,
                // color: Colors.pink,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                        right: 20.0,
                        left: 5.0,
                      ),
                      child: Text(
                        "Cappuccino",
                        style: TextStyle(
                          fontFamily: "Rubik",
                          fontSize: 15.0,
                          color: Colors.orange,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    textlistview("Espresso"),
                    textlistview("Black Coffee"),
                    textlistview("Mocha"),
                    textlistview("Americano"),
                    textlistview("Flat White"),
                    textlistview("Macchiato"),
                    textlistview("Cold Brew"),
                    textlistview("Frappe"),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                height: 375,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    listView(
                        "Cappuccino", "With oat Milk", "coffee2.jpg", "\$3.95"),
                    listView(
                        "Espresso", "With oat Milk", "coffee1.jpg", "\$4.12"),
                    listView("Latte", "With oat Milk", "coffee3.jpg", "\$2.36"),
                    listView(
                        "Cappuccino", "With oat Milk", "coffee4.jpg", "\$5.75"),
                    listView(
                        "Cappuccino", "With oat Milk", "coffee5.jpg", "\$7.54"),
                  ],
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0, left: 10.0, bottom: 10.0),
                child: Text(
                  "Special for you",
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'Rubik',
                    fontSize: 20.0,
                  ),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                height: 275,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    Container(
                      padding: EdgeInsets.all(15.0),
                      decoration: BoxDecoration(
                        color: Colors.black26,
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(20.0),
                            child: Image.asset(
                              "assets/coffeeImage/coffee5.jpg",
                              height: 250,
                            ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "5 Coffee Beans you\nMust Try!",
                                style: TextStyle(
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "Rubik",
                                  color: Colors.white,
                                ),
                                textAlign: TextAlign.start,
                              ),
                              SizedBox(
                                height: 30.0,
                              ),
                              Text(
                                "'People say money can’t\nbuy happiness.They Lie.\nMoney buys Coffee,\nCoffee makes You Happy!'",
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 13.0,
                                  fontFamily: "Rubik",
                                ),
                                textAlign: TextAlign.start,
                              ),
                              SizedBox(
                                height: 40.0,
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.of(context).push(
                                      MaterialPageRoute(builder: (context) {
                                    return CoffeeSecondPage();
                                  }));
                                },
                                child: Container(
                                  padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(
                                      20.0,
                                    ),
                                    color: Colors.orange,
                                  ),
                                  child: Text(
                                    "Buy Now",
                                    style: TextStyle(
                                      fontSize: 15.0,
                                      fontFamily: 'Rubik',
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

Widget listView(String text, String subtext, var imagename, String dollar) {
  return Container(
    margin: EdgeInsets.only(right: 15.0),
    padding: EdgeInsets.all(15.0),
    width: 200,
    decoration: BoxDecoration(
      color: Colors.black26,
      borderRadius: BorderRadius.circular(20.0),
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(20.0),
          child: Image.asset(
            "assets/coffeeImage/" + imagename,
            height: 250,
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          text,
          style: TextStyle(
            color: Colors.white,
            fontSize: 15.0,
            fontWeight: FontWeight.bold,
            fontFamily: "Rubik",
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          subtext,
          style: TextStyle(
            fontSize: 10.0,
            color: Colors.grey.shade500,
            fontFamily: "Rubik",
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              dollar,
              style: TextStyle(
                fontSize: 20.0,
                fontFamily: "Rubik",
                color: Colors.white,
              ),
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16.0),
                color: Colors.orange,
              ),
              child: Icon(
                Icons.add,
              ),
            ),
          ],
        )
      ],
    ),
  );
}

Widget textlistview(String textline) {
  return Container(
    margin: EdgeInsets.only(
      right: 20.0,
    ),
    child: Text(
      textline,
      style: TextStyle(
        fontFamily: "Rubik",
        fontSize: 15.0,
        color: Colors.white,
        fontWeight: FontWeight.bold,
      ),
    ),
  );
}
